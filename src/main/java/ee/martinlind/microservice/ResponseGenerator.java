package ee.martinlind.microservice;


import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import static java.lang.String.format;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;


/**
 * Gemerates JSON response from the JSON request.
 */
class ResponseGenerator {

    static final String FIRST_LEG = "a";
    static final String SECOND_LEG = "b";
    static final int INPUT_MAX_LENGTH = 8;
    static final int INPUT_MAX_DECIMAL_PLACES = 2;
    private final String request;

    ResponseGenerator(String request) {
        this.request = request;
    }

    String getResponse() {
        try {
            Map<String, Double> input = getInputAsMap();
            validateInput(input);
            return getResultAsJson(getArea(input));
        } catch (JsonSyntaxException e) {
            return handleMalformedJson();
        } catch (InvalidInputException e) {
            return handleInvalidInput(e);
        }
    }

    private void validateInput(Map<String, Double> input) throws InvalidInputException {
        double firstLeg;
        double secondLeg;

        try {
            firstLeg = input.get(FIRST_LEG);
            secondLeg = input.get(SECOND_LEG);
        } catch (Exception e) {
            String faultDetail = format(
                    "At least one of the inputs (%s, %s) is not a number.",
                    input.get(FIRST_LEG), input.get(SECOND_LEG));
            throw new InvalidInputException(faultDetail);
        }

        validateInputLength(firstLeg, secondLeg);
        validateInputDecimalPlaces(firstLeg, secondLeg);
    }

    private void validateInputLength(double firstLeg, double secondLeg)
            throws InvalidInputException {
        for (double eachLeg : Arrays.asList(firstLeg, secondLeg)) {
            String legAsString = Double.toString(eachLeg);

            if (legAsString.length() > INPUT_MAX_LENGTH) {
                String faultDetail = format(
                        "At least one of the inputs (%s, %s) is too long.",
                        firstLeg, secondLeg);
                throw new InvalidInputException(faultDetail);
            }
        }
    }

    private void validateInputDecimalPlaces(double firstLeg, double secondLeg)
            throws InvalidInputException {
        for (double eachLeg : Arrays.asList(firstLeg, secondLeg)) {
            String legAsString = Double.toString(eachLeg);

            String[] parts = legAsString.split("\\.");

            if (parts.length > 1 && parts[1].length() > INPUT_MAX_DECIMAL_PLACES) {
                String faultDetail = format(
                        "At least one of the inputs (%s, %s) contains too many decimal places.",
                        firstLeg, secondLeg);
                throw new InvalidInputException(faultDetail);
            }
        }
    }

    private Map<String, Double> getInputAsMap() {Gson gson = new Gson();
        return gson.fromJson(request, HashMap.class);
    }

    private String getResultAsJson(double area) {Map<String, String> result = new HashMap<>();
        result.put("area", format("%.2f", area));
        result.put("requestId", getRequestId());

        return new Gson().toJson(result);
    }

    private String handleMalformedJson() {
        String faultDetail = format("JSON of request '%s' cannot be parsed", request);
        return getFaultMessage("MalformedJSON", faultDetail);
    }

    private String handleInvalidInput(InvalidInputException e) {
        return getFaultMessage("InvalidInput", e.getMessage());
    }

    private String getFaultMessage(String faultCode, String faultDetail) {
        Map<String, String> result = new HashMap<>();
        result.put("faultCode", faultCode);
        result.put("faultDetail", faultDetail);
        result.put("requestId", getRequestId());

        return new Gson().toJson(result);
    }

    private double getArea(Map<String, Double> input) {
        return (input.get(FIRST_LEG) * input.get(SECOND_LEG)) / 2.0;
    }


    /**
     * XXX: Actually this functionality should be somewhere else and ID generation should be
     * implemented in unique and thread-safe manner, but for simplicity we use simply combination
     * of nano time and current thread name assuming that this is unique enough for us.
     */
    private String getRequestId() {
        return format("%d-%s", System.nanoTime(), Thread.currentThread().getName());
    }

    private static class InvalidInputException extends Exception {
        private InvalidInputException(String message) {
            super(message);
        }
    }
}
