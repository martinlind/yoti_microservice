# Creating microservice application for calculating area of the right triangle

## Goal

Create JSON microservice that takes lengths of the two legs and gives area in return

Input fields in JSON message:
* **a** - first leg of the triangle - max length of 8 characters and a maximum precision of 2 decimal places
* **b** - second leg of the triangle - max length of 8 characters and a maximum precision of 2 decimal places

Example input message:

```json
{
	'a': 4.0,
	'b': 4.5
}
```

Response may be both answer and fault response.

Fields in answer message:
* **area** - area of the rectangle as an answer
* **requestId** - request ID **TODO** - generation mechanism will be discussed!

Example answer message:
```json
{
	'area': 9.0,
	'requestId': 111
}
```

Example fault message:
```json
{
	'faultCode': 'MalformedJSON'
	'faultDetail': 'JSON of request \'random\' cannot be parsed'
	'requestId': 111
}
```

## TODO list

+ Get initial server up and running
	+ Create project structure using Gradle scripts
	+ Find code to set up server
	+ Make sure that You can actually send the request and get the response.
+ Create initial tests for the server.
+ Implement functionality of the server
	+ First success case
	+ Non-JSON
	+ Not-a-number in request
	+ Missing request field
	+ Too long input
	+ Too many decimal places
	+ ID generation (exact datetime + thread)
- Packaging
	- Executable JAR
	* Instructions to use
* Improvements for tests
	* Read URL from file
* User guide
* Closing
	* Send E-Mail about repository

## Testing (quick)

```sh
curl -raw -v http://localhost:8081/json --data-binary @requests/req_correct.json -H "Content-Type: application/json; charset=UTF-8"
```
