#!/usr/bin/env ruby
# encoding:utf-8
require 'json'

test_data = [
  {request: "{'a': 4, 'b': 4}", response: {area: 8}},
  {request: "random", response: 
      {faultCode: "MalformedJSON", faultDetail: "JSON of request 'random' cannot be parsed"}},
  {request: "{'a': 'z', 'b': 4}", response: 
      {faultCode: "InvalidInput", faultDetail: "At least one of the inputs (z, 4.0) is not a number."}},
  {request: "{'a': 4}", response: 
      {faultCode: "InvalidInput", faultDetail: "At least one of the inputs (4.0, null) is not a number."}},
  {request: "{'a': 7777777777, 'b': 4}", response: 
      {faultCode: "InvalidInput", faultDetail: "At least one of the inputs (7.777777777E9, 4.0) is too long."}},
  {request: "{'a': 123.456, 'b': 4}", response: 
      {faultCode: "InvalidInput", faultDetail: "At least one of the inputs (123.456, 4.0) contains too many decimal places."}}
]

# Assumes curl is installed

def get_response(request)
  # XXX: Rather ugly solution. On top of that assumes that cURL is installed.
  response = `curl http://127.0.0.1:8081/json --data-binary "#{request}" -H "Content-Type: application/json; charset=UTF-8"`

  unless $?.success?
    raise "Exit status for request unsuccessful: '#{$?.exitstatus}', test failed. Request:\n#{request}"
  end

  response
end

def run_testcase(request, expected_response)
  puts " -- Testing for request '#{request}' - start --- "

  passed = false
  response_as_json = JSON.parse(get_response(request))

  if expected_response.has_key?(:area) 
    if response_as_json["area"] == nil
      STDERR.puts("Empty response")
    elsif expected_response[:area] == response_as_json["area"].to_f
      passed = true
    else
      STDERR.puts("Area expected to be '#{expected_response[:area]}', but was '#{response_as_json["area"]}'")
    end
  elsif expected_response.has_key?(:faultCode)
    if response_as_json["faultCode"].eql?(expected_response[:faultCode]) && \
        response_as_json["faultDetail"].eql?(expected_response[:faultDetail])
      puts ":::Condition true for fault code '#{response_as_json["faultCode"]}'" # XXX
      puts ":::Condition true for expected fault code '#{expected_response[:faultCode]}'" # XXX

      passed = true
    else
      STDERR.puts("Actual response '#{response_as_json}' and expected "\
          "response '#{expected_response}' do not match.")
    end
  else
    STDERR.puts("Response '#{response_as_json}' is neither response or fault message")
  end

  { passed: passed, request: request }
rescue JSON::ParserError
  STDERR.puts "JSON parser error occurred: #{$!.message}"
rescue Exception
  STDERR.puts "Request sending/processing error occurred: #{$!.message}"
ensure
  puts " -- Testing for request '#{request}' - finished --- "
  return { passed: passed, request: request }
end

failed_tests = []

test_data.each do |each_test|
  result = run_testcase(each_test[:request], each_test[:response])

  unless result[:passed]
    failed_tests << result 
  end
end

puts " ------ SUMMARY ------ "

unless failed_tests.empty?
  STDERR.puts("ERROR: There were failing tests: ")
  failed_tests.each do |each_failed|
    puts " --- REQ: '#{each_failed[:request]}'"
  end
else
  puts "SUCCESS: All tests passed"
end
